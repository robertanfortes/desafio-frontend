# Desafio Frontend
Crie uma aplicação utilizando as Api's do portal Developers da Marvel.

# Quick start

## Instalar dependencias via npm
$ npm install

## Start do servidor
$ npm start

ir para [http://localhost:3000](http://localhost:3000)

## Dependencies

* `node` and `npm`

## Developing

### Build files

* single run: `npm build`

## Testing

#### Unit Tests

* single run: `npm test`
