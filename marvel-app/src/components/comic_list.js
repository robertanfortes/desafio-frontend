import React from 'react';
import Modal from '../components/comic_modal';
import $ from 'jquery';

const PUBLIC_KEY = '10360044d0ec56129f1f4695297f414b'
const limit = 100

// overwrite style
const modalStyle = {
	overlay: {
		backgroundColor: 'rgba(0, 0, 0,0.5)'
	}
};

const mainStyle = {
	button: {
		backgroundColor: '#000',
		border: '1px solid transparent',
		color: '#fff',
		margin: '0 auto',
		display: 'block',
	},
  span: {
    display: 'block',
  }
};

class ComicList extends React.Component {

  constructor(props,creators) {
    super(props)
    this.state = {
      data: [],
      isModalOpen: false,
      isInnerModalOpen: false,
      requestSent: false,
			creators:creators
    }
    // bind functions
    this.closeModal = this.closeModal.bind(this);
    this.getComicList = this.getComicList.bind(this);
		this.limitCharacter = this.limitCharacter.bind(this);
		// this.getPrice = this.getPrice.bind(this);

  }

  componentDidMount() {
    this.getComicList();
		window.addEventListener('scroll', this.handleOnScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleOnScroll);
  }
	// get Ironman's comics list from API
  getComicList = (dynamicData) => {
	  return fetch(`https://gateway.marvel.com/v1/public/characters/1009368/comics?format=comic&limit=${limit}&apikey=${PUBLIC_KEY}`)
	    .then((response) => response.json())
	    .then((responseJson) => {
	      this.setState({
	        data:responseJson.data.results
	      })
				this.limitCharacter();
	    })
  }

  // open modal with selected comic informations
  getComicItem = (dynamicData,creators) => {
		this.getCreators(dynamicData)
    this.setState({
      isModalOpen: true,
      title:dynamicData.title,
      series:dynamicData.series.name,
      src:`${dynamicData.thumbnail.path}/portrait_xlarge.${dynamicData.thumbnail.extension}`,
      description:dynamicData.description,
			pages:dynamicData.pageCount
    })
		// this.getPrice(dynamicData);
  }

	getCreators = (dynamicData) => {
		if (dynamicData !== undefined) {
			let creators = dynamicData.creators.items
			creators.map((creators,index) =>  this.setState({
				name:creators.name
			}))
		}
	}

  // close modal
  closeModal= () => {
    this.setState({
      isModalOpen: false
    })
  }
	// infinite scroll + lazyload
  handleOnScroll = () => {
    $(document).ready(function(){
      $(".comic-item").slice(0, 8).addClass('display');
      var scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
      var scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;
      var clientHeight = document.documentElement.clientHeight || window.innerHeight;
      var scrolledToBottom = Math.ceil(scrollTop + clientHeight) >= scrollHeight;
      if (scrolledToBottom) {
	      setTimeout(function(){
	      	$(".comic-item:hidden").slice(0, 8).addClass('display');
	    	}, 1000);
				var totalComics = $('.comic-item.display').length;
				if (totalComics === 100) {
					$(".loader").addClass("display-none")
				}
      }
    });
  }
	// limit character's quantity
	limitCharacter = () => {
    var showChar = 40;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "more";
    $('.more').each(function() {
      var content = $(this).html();
      if(content.length > showChar) {
        var c = content.substr(0, showChar);
        var h = content.substr(showChar, content.length - showChar);
        var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink" onClick = {this.getComicItem.bind(this, dynamicData)}> ' + moretext + '</a></span>';
        $(this).html(html);
    	}
    });
    $(".morelink").click(function(event){
      event.preventDefault();
    });
	}

  render() {
    return (
      <div className="container">
        <div style={mainStyle.app}>
          <Modal
            isModalOpen={this.state.isModalOpen}
            closeModal={this.closeModal}
            style={modalStyle}>
              <img
              src={this.state.src}
              alt={this.state.title}/>
              <h2 className="comic-title">{this.state.title}</h2>
              <span className="comic-series tx-align-left">Series: {this.state.series}</span>
							<span className="comic-creator tx-align-left">Creator: {this.state.name}</span>
							<span className="comic-creator tx-align-left">Pages: {this.state.pages}</span>
              <span className="comic-description tx-align-left">{this.state.description}</span>
              <button style={{
                  ...mainStyle.button,
                  margin: 0,
                  width: 'auto',
                  marginTop: 10
                }} onClick={this.closeModal}
								className="btn-close-modal">X
              </button>
          </Modal>
        </div>
        <ul className="flex-grid-4 comic-list">
          {
            this.state.data.map( (dynamicData,key)=>
              <li key={dynamicData.id} onClick = {this.getComicItem.bind(this, dynamicData)}
                className="col comic-item">
                {this.handleOnScroll(dynamicData)}
                <img
                src={`${dynamicData.thumbnail.path}/portrait_xlarge.${dynamicData.thumbnail.extension}`}
                alt={dynamicData.title}
                className="imgs"/><br/>
                <h2 className="comic-title">{dynamicData.title}</h2>
                <span className="comic-description more">{dynamicData.description}</span>
              </li>
            )
          }
        </ul>
        <div className="loader"></div>
      </div>
      )
    }
}


export default ComicList;
