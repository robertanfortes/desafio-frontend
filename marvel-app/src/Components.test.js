import React from 'react';
import ReactDOM from 'react-dom';
import ComicList from '../src/components/comic_list';
import Modal from '../src/components/comic_list';

describe('ComicList', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<ComicList/>, div);
    });
});

it('renders without crashing', () => {
  const li = document.createElement('li.comic-item');
  ReactDOM.render(<ComicList/>, li);
});

describe('Modal', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Modal/>, div);
    });
});
