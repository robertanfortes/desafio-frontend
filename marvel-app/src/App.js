import React from 'react';
import ComicList from './components/comic_list';

class App extends React.Component {

  render() {
    return (
      <div>
        <h1 className="app-title">Iron Man Comics - Marvel</h1>
          <ComicList>

          </ComicList>
      </div>
    );
  }
}

export default App;
