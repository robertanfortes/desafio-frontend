{
    "moduleFileExtensions": [
        "js",
        "jsx"
    ],
    "moduleNameMapper": {
        "ComicList": "<rootDir>/src/components/comic_list.js",
        "Modal": "<rootDir>/src/components/comic_modal.js"
    }
}
